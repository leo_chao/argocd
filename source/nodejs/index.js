var express = require('express');
var app = express();
app.get('/', function(req, res) {
  res.send('Hello World! port 6000');
});
var server = app.listen(6000, function() {
  var host = server.address().address;
  var port = server.address().port;
  
  console.log("Example app listening at 'http://%s:%s'", host, port);
})
